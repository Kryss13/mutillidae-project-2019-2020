# Projet Mutillidae

## Initialisation de l'environnement :

```bash
git clone -o projet-devsecop2019-2020 https://gitlab.com:ynov-devops/2019-2020/projects/devsecops.git

cd projet-devsecop2019-2020
sudo chown root -R mutillidae
```

## Installation Mutillidae


```bash
docker network create owasp


# https://hub.docker.com/r/szsecurity/mutillidae/
docker run --rm -d -p 31000:80 -v $(pwd)/mutillidae --cpus="1" --net owasp --name mutillidae szsecurity/mutillidae

docker run --rm --net owasp -i owasp/zap2docker-stable zap-cli quick-scan -l Informational -s all --self-contained --start-options '-config api.disablekey=true' --ajax-spider -r http://mutillidae
```


## Aller plus loin avec Dvwa

```bash
# https://hub.docker.com/r/vulnerables/web-dvwa/
docker run --rm -d -p 31001:80 --net owasp --name dwa vulnerables/web-dvwa

docker run --rm --net owasp -i owasp/zap2docker-stable zap-cli quick-scan --self-contained --start-options '-config api.disablekey=true' --spider -r http://dwa
```

## Clean UP


```bash
docker network rm owasp
```

>## Install Docker 

Initialisation, instalation de Docker, de Zaproxy, ainsi que les dépendances associés

>## Problème de push 
>>### Impossible de push sur le repo de ma vm

root@ubuntu:/home/kryss/mutillidae-project-2019-2020-master:$ git push -u origin master  
Username for 'https://gitlab.com': Kryss13  
Password for 'https://Kryss13@gitlab.com':  
To https://gitlab.com/Kryss13/Mutillidae-project-2019-2020.git
 ! [rejected]        master -> master (non-fast-forward)
error: impossible de pousser des références vers 'https://gitlab.com/Kryss13/Mutillidae-project-2019-2020.git'  
astuce: Les mises à jour ont été rejetées car la pointe de la branche courante est derrière  
astuce: son homologue distant. Intégrez les changements distants (par exemple 'git pull ...')  
astuce: avant de pousser à nouveau.  
astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.



>## Nouvelle instalation sur Kali

Suite à tout mes problème rencontré sur Ubuntu, j'ai décider de changer de vm et de passer sur Kali (qui inclu deja Zap)  
Donc j'ai installer Docker, visual studio, et télécharger le projet sur cette nouvelle vm.


>## Problème lors du push corrigé ! 

J'ai eu les même soucis lors du push sur la nouvelle vm... Même en faisant git pull, git fetch, rebase rien ne fonctionnait   
Par contre en regardant sur Stackoverflow, j'ai trouvé cette commande : 
```git pull origin branchname --allow-unrelated-histories```  
Et la cela à fonctionné ! Je peux donc continuer et lancé mes test et mes corrections 

>## Corection X-Frame-Options

Afin d'éviter les attaques de type 'clickjacking', les sites peuvent utiliser cet en-tête pour s'assurer que leur contenu ne soit pas embarqués dans d'autres sites  

J'ai décidé de corriger les erreurs dues aux X-Frame-Options sur les pages Database-offline.php, index.php et login.php  

Je me suis donc rendu sur les pages cité précédemment pour rajouter l'en-tête de réponse http : X-Frame-Options : SAMEORIGIN
Deux options existent pour les options du x-frame :
* Sameorigine : La page peut être affichée que dans une frame avec une origine qui est la même que la page elle-même.
* Deny : La page ne peut pas être affichée dans une frame, quand bien même un site tiers tenterais de la charger.
